package com.mmr.rabbitmq.util.workfair;

import com.mmr.rabbitmq.util.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author 董文登
 * @date 2020/3/22
 */
public class Send {

    private static final String QUEUE_NAME = "test_work_queue";

    public static void main(String[] args) throws Exception {
        // 获取连接
        Connection connection = ConnectionUtils.getConnection();
        // 获取channel
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        /**
         * 每个消费者发送确认消息之前，消息队列不发送下一个消息到消费者，一次只处理一个消息
         * 限制发送给同一个消费者，不得不超过一条消息
         */
        int prefetchCount = 1;
        channel.basicQos(prefetchCount);
        // 循环发送消息
        for (int i = 0; i < 50; i++) {
            String msg = "hello" + i;
            System.out.println("[WQ ] send:" + msg);
            channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
            Thread.sleep(i * 5);
        }
        // 关闭资源
        channel.close();
        connection.close();
    }
}
