package com.mmr.rabbitmq.util.work;

import com.mmr.rabbitmq.util.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author 董文登
 * @date 2020/3/22
 */
public class Send {

    /**
     * 模型
     *                  |--C1
     * P-----Queue------|
     *                  |--C2
     */

    private static final String QUEUE_NAME = "test_work_queue";

    public static void main(String[] args) throws Exception {
        // 获取连接
        Connection connection = ConnectionUtils.getConnection();
        // 获取channel
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 循环发送消息
        for (int i = 0; i < 50; i++) {
            String msg = "hello" + i;
            System.out.println("[WQ ] send:" + msg);
            channel.basicPublish("", QUEUE_NAME, null, msg.getBytes());
            Thread.sleep(i * 20);
        }
        // 关闭资源
        channel.close();
        connection.close();
    }
}
