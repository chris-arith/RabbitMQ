package com.mmr.rabbitmq.util.confirm;

import com.mmr.rabbitmq.util.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author 董文登
 * @date 2020/3/23
 */
public class Send2 {

    private static final String QUEUE_NAME = "test_queue_confirm2";

    public static void main(String[] args) throws Exception {
        // 获取连接
        Connection connection = ConnectionUtils.getConnection();
        // 获取channel
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        // 生产者调用confirmSelect方法将channel设为confirm模式
        channel.confirmSelect();

        // 发送消息
        String msgString = "hello confirm message batch!";
        // 批量confirm
        for (int i = 0; i < 10; i++) {
            channel.basicPublish("", QUEUE_NAME, null, msgString.getBytes());
        }
        // 确认
        if (!channel.waitForConfirms()) {
            System.out.println("message send failed");
        } else {
            System.out.println("message send ok");
        }
        channel.close();
        connection.close();
    }

}