package com.mmr.rabbitmq.util.tx;

import com.mmr.rabbitmq.util.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author 董文登
 * @date 2020/3/23
 */
public class TxSend {

    private static final String QUEUE_NAME = "test_queue_tx";

    public static void main(String[] args) throws Exception {
        // 获取连接
        Connection connection = ConnectionUtils.getConnection();
        // 获取channel
        Channel channel = connection.createChannel();
        // 声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        String msgString = "hello tx message";

        try {
            // 开启事务模式
            channel.txSelect();
            int i = 1/0;
            channel.basicPublish("", QUEUE_NAME, null, msgString.getBytes());
            channel.txCommit();
            System.out.println("Send :" + msgString);
        } catch (Exception e) {
            // 有异常则回滚事务
            channel.txRollback();
            System.out.println(" send message rollback");
        }

        channel.close();
        connection.close();


    }
}
