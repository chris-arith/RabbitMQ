package com.mmr.rabbitmq.util.topic;

import com.mmr.rabbitmq.util.ConnectionUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @author 董文登
 * @date 2020/3/23
 */
public class Send {

    private static final String EXCHANGE_NAME = "test_exchange_topic";

    public static void main(String[] args) throws Exception {
        // 获取连接
        Connection connection = ConnectionUtils.getConnection();
        // 获取channel
        Channel channel = connection.createChannel();
        // 声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        // 发送消息
        String msgString = "商品......";
        channel.basicPublish(EXCHANGE_NAME, "goods.delete", null, msgString.getBytes());
        System.out.println("---send :" + msgString);

        channel.close();
        connection.close();
    }
}
